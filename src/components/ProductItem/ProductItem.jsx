import React, { Component } from "react";
import styles from "./ProductItem.module.css";
import base from "../../ulti/base";

export default class ProductItem extends Component {
  render() {
    const { product, handleAdd, handleView } = this.props;
    return (
      <div className="col m-0">
        <div className={styles.card}>
          <img
            src={product.image}
            className="card-img-top w-100 d-block"
            alt={product.image}
          />
          <div className="card-body px-2">
            <p>{base.changeText(product.name, 10)}</p>
            <p className={styles.textDesc + " text-secondary text-break"}>
              {base.changeText(product.alias, 25)}
            </p>
            <p className="text-danger text-end">
              {product.price.toLocaleString() + "$"}
            </p>
          </div>
          <div className={styles.cardFooter + "py-2"}>
            <button
              onClick={() => handleAdd(product)}
              className={styles.btn + " btn btn-primary"}
            >
              Add
            </button>
            <button
              onClick={() => handleView(product)}
              className={styles.btn + " btn btn-success"}
            >
              View
            </button>
          </div>
        </div>
      </div>
    );
  }
}
