import React, { Component } from "react";
import data from "../../data.json";
import ProductList from "../ProductList/ProductList";
import Modal from "../Modal/Modal";
import Cart from "../Cart/Cart";
import base from "../../ulti/base";
import { message } from "antd";

export default class ShoesStore extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: data,
      modal: [], // cart
      productDetail: data[0],
    };
  }

  setStateModal = (listCart = []) => {
    this.setState({ modal: listCart });
  };

  addToCart = (product) => {
    let cloneCart = [...this.state.modal];
    const index = base.searchFromList(cloneCart, product.id);
    if (index === -1) {
      let newProduct = { ...product, number: 1, total: product.price };
      cloneCart.push(newProduct);
    } else {
      this.handleInscrease(+1, cloneCart[index].id);
    }
    this.setState({ modal: cloneCart });
  };

  setStateDetail = (data = {}) => {
    this.setState({ productDetail: data });
  };

  handleRemove = (id = -1) => {
    let cloneCart = [...this.state.modal];
    let index = base.searchFromList(cloneCart, id);
    if (cloneCart.length === 0 && index === -1 && id === -1) return;

    index !== -1 &&
      cloneCart.splice(index, 1) &&
      message.open({ type: "success", content: "Xóa thành công" });
    this.setState({ modal: cloneCart });
  };

  handleInscrease = (option = 0, id) => {
    let cloneCart = [...this.state.modal];
    let index = base.searchFromList(cloneCart, id);

    if (cloneCart.length === 0 && index === -1 && id === -1) return;

    // Tăng / Giảm số sản phẩm trong cart
    if (cloneCart[index].number >= 1) {
      cloneCart[index].number += option;
      cloneCart[index].total = cloneCart[index].price * cloneCart[index].number;
    }
    // Xóa shoe trong cart khi số sản phẩm == 0
    cloneCart[index].number <= 0 &&
      cloneCart.splice(index, 1) &&
      message.open({ type: "success", content: "Xóa thành công" });

    this.setState({ modal: cloneCart });
  };
  render() {
    return (
      <div className="container-fluid">
        <div className="row">
          <Cart
            listCart={this.state.modal}
            handleRemove={this.handleRemove}
            handleInscrease={this.handleInscrease}
            setStateModal={this.setStateModal}
          ></Cart>
          <ProductList
            productsData={this.state.products}
            setStateDetail={this.setStateDetail}
            addToCart={this.addToCart}
            handleInscrease={this.handleInscrease}
          ></ProductList>
          <Modal productDetail={this.state.productDetail}></Modal>
        </div>
      </div>
    );
  }
}
