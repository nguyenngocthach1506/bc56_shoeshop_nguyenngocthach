import React, { Component } from "react";
import ProductItem from "../ProductItem/ProductItem";

export default class ProductList extends Component {
  renderProductItem = (list = []) => {
    return list.map((item, index) => (
      <ProductItem
        key={"product" + index}
        product={item}
        handleView={this.props.setStateDetail}
        handleAdd={this.props.addToCart}
        handleInscrease={this.props.handleInscrease}
      ></ProductItem>
    ));
  };
  render() {
    return (
      <div className="col-6">
        <div className="row row-gap-3 row-cols-2 row-cols-lg-3 bg-light p-2">
          {this.renderProductItem(this.props.productsData)}
        </div>
      </div>
    );
  }
}
