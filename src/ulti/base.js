function changeText(text = "", number) {
    text = text.length <= number ? text : text.slice(0, number) + "...";
    return text;
};


function searchFromList(list = [], id) {
    let index = list.findIndex((item) => item.id === id)
    return index;
}

const base = {
    changeText: changeText,
    searchFromList: searchFromList,
}

export default base;